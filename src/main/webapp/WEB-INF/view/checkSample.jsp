<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<p>${message}</p>

<!-- 今回は「action」を省略せずに記述してます。しかし「method」は省略されているため、
「/SpringKenshu/sample/check/info/」にPOSTリクエストを送信するという形になります。 -->

<form:form modelAttribute="checkForm" action="${pageContext.request.contextPath}/sample/check/info/">
    <form:checkboxes path="employees" items="${checkEmployees}" delimiter="/" />
    <input type="submit" />
</form:form>
</body>
</html>