<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<p>${message}</p>
<form:form modelAttribute="radioForm" action="${pageContext.request.contextPath}/sample/radio/info/">
    <!-- ラジオボタンの生成は「form:radiobutton」タグを使用
    	この記述で、RadioFormの「name」フィールドにチェックした項目のvalue値がセットされる。-->
    <form:radiobuttons path="name" items="${checkEmployees}" />
    <input type="submit" />
</form:form>
</body>
</html>